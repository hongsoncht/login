//
//  ViewController.swift
//  signIn-SDK
//
//  Created by Tran Son on 11/3/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FacebookShare
import GoogleSignIn

class ViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
 
    @IBAction func SignInFacebook(_ sender: Any) {
        let manager = LoginManager()
        manager.logIn(readPermissions: [.publicProfile, .email], viewController: self)
        {
            (result) in
            switch result {
            case .cancelled:
                print("cancel")
                break
            case.failed(let error) :
                print("failed == \(error.localizedDescription)")
                break
            case.success(let grantedPermission, let declinedPermission, let  accessToken):
                print("access token == \(accessToken)")
            }
        }
    }
    
    @IBOutlet weak var signInGG: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        signInGG.addTarget(self, action: #selector(signinUser(_:)), for: .touchUpInside)
    }
    
    @objc func signinUser(_ sender: UIButton)
    {
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.uiDelegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
                
        }
    }
    
}

